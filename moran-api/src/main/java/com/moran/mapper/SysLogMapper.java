package com.moran.mapper;

import com.moran.util.MyMapper;

import com.moran.model.SysLog;

/**
 * sys_log - 登录日志
 *
 * @author 系统自动生成
 */
@org.apache.ibatis.annotations.Mapper
public interface SysLogMapper extends MyMapper<SysLog, Integer> {

}